# A standalone LORENE pointwise exporter code

This repository holds a code to export Initial Data created with the
[LORENE](http://www.lorene.obspm.fr/) `Magstar`, `rotstar` or
`coal` (BNS) codes at arbitrary
*spatial points*. This is a slight modification of the `MagNS` and
`BinNS` exporter classes (shipped with LORENE), they only allow
to export to an array of points.

## Comparison with MagNS
The `MagNS` code exports the content of a single magnetized neutron
star spacetime, ie. the

  * lapse, shift
  * 3-curvature and extrinsic curvature
  * Baryon and specific energy density (nbar)
  * 3-velocity, Magnetic field and 4-current

to a cartesian grid. It provides a signature so you can pass an array
of your coordinates and the `resu.d` file created by `magstar` and it
evaluates the spectral fields on these coordinates on especially
created arrays.

In contrast, this code allows you to call the interpolation for any
spatial position `(x,y,z)` on your own. Output is then written not into
a continously storage but just into a structure which holds the
initial data at the given position.

This code is helpful for our [ExaHyPE](http://exahype.eu/) code
where we have exactly this signature for reading in initial data.
Fields can be evaluated in parallel, ie. with using `TBB` or `OMP`,
or of course with `MPI` in several processes as it is done in the
EinsteinToolkit.

This repository is designed to provide similar options as the
[twopunctures-standalone](https://bitbucket.org/relastro/twopunctures-standalone/)
initial data code, ie. a lightweight C++ and Fortran API and some
underlying library.

## Dependencies

With LORENE, it's not that easy to decouple all the dependencies. You
should install LORENE on your computer and make sure you have the
libraries `fftw3`, `lapack`, `blas`, `gsl` available. In contrast, you
*do not* need PGPLOT when you just remove any calls to PGPLOT, ie. in
`magstar`. This standalone exporter code also don't need PGPLOT, of
course.

## How to start

First, you need a working copy of LORENE. You can obtain one via Git
at [bitbucket.org/relastro/lorene](https://bitbucket.org/relastro/lorene)
or via [the LORENE website](http://www.lorene.obspm.fr/)  (which holds
extensive material how to install LORENE). We also provide instructions
how to start with Lorene at our [Lorene-tools repository](https://bitbucket.org/relastro/lorene-tools/).

In short, this should do it for you:

```
# Download all the repositories
mkdir LoreneID && cd LoreneID
git clone https://bitbucket.org/relastro/lorene.git Lorene
git clone https://bitbucket.org/relastro/lorene-pointwiseexport.git Lorene-PointwiseExport
git clone https://bitbucket.org/relastro/lorene-tools.git Lorene-Tools
cd Lorene

export HOME_LORENE=$PWD
export LORENE_HOME=$PWD

# This works fine on a Laptop:
sudo apt-get install liblapack-dev pgplot5 gsl-dev libblas-dev
ln -s ../Lorene-Tools/local_settings_ubuntu local_settings
make test && echo all fine && cd ..
```

Then you can try out the examples, for instance

```
cd Lorene-PointwiseExport/examples/bhb-lp-tovrot-example/
./run-example.sh
```

This will build the `rotstar` code and create the initial data of a (probably rotating)
Neutron Star with a specific EOS (in fact the [BHB-ΛΦ](https://arxiv.org/abs/1404.6173)
equation of state) and read in the initial data with an exemplaric Fortran interface.


Author: SvenK, 2017-02-17, 2018-08-24










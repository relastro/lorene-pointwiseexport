#!/bin/bash
#
# This is an example invocation to create Lorene initial data for
# Rotating neutron stars. This takes a few seconds on a laptop. In contrast,
# creating BNS data takes several hours and should run on a cluster.
# We have finished data available in the group and you can just get
# them instead of creating your own.
#

cd "$(dirname $0)"
set -e

[ -e resu.d ] || {
	[ -e rotstar ] || {
		echo "Trying to build rotstar..."
		[ -z "$HOME_LORENE" ] && { echo "You must set the HOME_LORENE environment variable"; exit -1; }
		Rot_star="$HOME_LORENE/Codes/Rot_star/"
		[ -d $Rot_star ] || { echo "$Rot_star is not a directory, is HOME_LORENE correct?"; exit -1; }
		[ -e $Rot_star/rotstar ] || ( cd $Rot_star; make rotstar; )
		ln -vfs $Rot_star/rotstar
	}

	echo "Running rotstar"
	./rotstar
	echo "Done creating initial data."
}

[ -e lorene-fortran-example ] || {
	src="../../src/"
	( cd $src/lorene-pointlib-fortran/ ; make )
	( cd $src/example-standalone-fortran/ ; make )
	ln -s $src/example-standalone-fortran/lorene-fortran-example
}

echo "Run it"
./lorene-fortran-example

echo "Finished successfully"

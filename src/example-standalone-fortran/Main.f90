Program FortranIntegrationDemo
	USE Lorene_Export, ONLY : Lorene_Export_Setup, Lorene_Export_Interpolate
	IMPLICIT NONE
	INTEGER :: i
	REAL :: x(3), Q(20)
	! Interpolation parameters
	REAL, PARAMETER :: xmin = 0.0, xmax = 15.0
	INTEGER, PARAMETER :: numpoints = 20

	PRINT *, "This is a demonstrator showing how Fortran functions might"
	PRINT *, "call C functions to interpolate Initial Conditions (Initial Data)"
	PRINT *, "on any cartesian grid"

	CALL Lorene_Export_Setup()
	
	x = 0
	DO i=1,numpoints
		x(1) = xmin + (xmax - xmin) * i / numpoints
		PRINT *, "Requesting initial data at ", x
		
		Q = 0
		CALL Lorene_Export_Interpolate(x, Q)
		PRINT *, "Got back Initial data: "
		PRINT *, Q
	ENDDO
	

END ! Program FortranIntegrationDemo


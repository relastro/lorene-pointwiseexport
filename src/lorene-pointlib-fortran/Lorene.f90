!
! This is a Fortran adapter to the Lorene Cartesian pointwise exporter code.
! This is only needed when you want to use to readin Lorene initial data
! to your Fortran code.
!

MODULE LORENE_EXPORT
	IMPLICIT NONE

	INTERFACE
		SUBROUTINE LORENE_EXPORT_SETUP() BIND(C)
			USE, INTRINSIC :: ISO_C_BINDING
			IMPLICIT NONE
		END SUBROUTINE LORENE_EXPORT_SETUP
	END INTERFACE
	
	INTERFACE
		SUBROUTINE LORENE_EXPORT_INTERPOLATE(x,Q) BIND(C)
			USE, INTRINSIC :: ISO_C_BINDING
			IMPLICIT NONE
			REAL, INTENT(IN)               :: x(3)
			REAL, INTENT(OUT)              :: Q(20) ! Tune this to your length of the conserved quantities
		END SUBROUTINE LORENE_EXPORT_INTERPOLATE
	END INTERFACE
	
END MODULE LORENE_EXPORT


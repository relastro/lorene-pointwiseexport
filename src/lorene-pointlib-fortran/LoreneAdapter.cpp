/**
 * This is a plain C adapter to the C++ RNSID code which is also suitable
 * for being run from Fortran.
 **/

#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "lorene-pointlib/UnitConverter.h"
#include "lorene-pointlib/BinNS.h"
#include "lorene-pointlib/MagNS.h"
#include "lorene-pointlib/RotNS.h"

using namespace LorenePointwise;
using namespace std;

// Please choose here which kind of ID you want.
// You can choose between BinNS, MagNS and RotNS
#define IDType RotNS

IDType* ns;

#define STRME(name) #name
#define IDTypeAsString STRME(IDType)

extern "C" {

void lorene_export_setup() {
	if(ns) {
		cerr << "Error, Lorene was already setup." << endl;
		abort();
	}
	
	// Having a file named "resu.d" is standard for Lorene. For simplicity, here we assume
	// it to be in the current working directory (you may use softlinks), since the file
	// "eos_akmalpr.d" has to be in the current working directory for Lorene-internal reasons,
	// too. So we now have two files which have to be located in the current working directory :)
	const char* filename = "./resu.d";
	
	cout << "Reading " << IDTypeAsString << " initial data from file " << filename << " ... " << endl;

	ns = new IDType(filename);
}

void lorene_export_interpolate(const double* x, double* Q) {
	if(!ns) {
		cerr << "You must run Lorene_Export_Setup() before starting to interpolate" << endl;
		abort();
	}
	
	LorenePointwise::idvars id;
	UnitConverter<IDType>(*ns).interpolate(x, id);
	// cout << "Interpolation result: " << id.toString() << endl;

	// Copy values from the idvars structure to the state vector Q, according to the
	// linearization you have. The following example is given for the ExaHyPE GRMHD application.
	
	Q[0] = id.nbar; // baryon rest mass density
	Q[1] = id.u_euler_x; // velocity
	Q[2] = id.u_euler_y;
	Q[3] = id.u_euler_z;
	Q[4] = id.ener_spec; // specific internal energy
	Q[5] = 0; // Magnetic field
	Q[6] = 0;
	Q[7] = 0;
	Q[8] = 0; // Psi
	Q[9] = id.nnn; // Lapse
	Q[10] = id.beta_x; // Shift
	Q[11] = id.beta_y;
	Q[12] = id.beta_z;
	Q[13] = id.g_xx; // Metric
	Q[14] = id.g_xy;
	Q[15] = id.g_xz;
	Q[16] = id.g_yy;
	Q[17] = id.g_yz;
	Q[18] = id.g_zz;
}

}// extern "C"

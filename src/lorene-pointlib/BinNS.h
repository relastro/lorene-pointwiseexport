#ifndef __LORENE_POINTWISE_READER_BIN_NS__
#define __LORENE_POINTWISE_READER_BIN_NS__

#include <stdio.h> // FILE*

#include "lorene-pointlib/forward-lorene.h"
#include "lorene-pointlib/idvars.h"

namespace LorenePointwise {
    class BinNS;
}

/**
 * Started in 2017-05-19, in order to prepare the reader for the
 * advanced-beyond-MagNS reader.
 * Based on the BinNS Exporter class in Lorene.
 **/
class LorenePointwise::BinNS  {
    // Data, as in MagNS:
    // -----
    public:
	/// Eos name star 1
	char eos_name1[100] ;

	/// Adiabatic index of EOS 1 if it is polytropic (0 otherwise)
	double gamma_poly1 ;

	/**
	*  Polytropic constant of EOS 1 if it is polytropic (0 otherwise)
	*  [unit: $\rho_{\rm nuc} c^2 / n_{\rm nuc}^\gamma$]
	*/
	double kappa_poly1 ;

	/// Eos name star 2
	char eos_name2[100] ;

	/// Adiabatic index of EOS 2 if it is polytropic (0 otherwise)
	double gamma_poly2 ;

	/**
	*  Polytropic constant of EOS 2 if it is polytropic (0 otherwise)
	*  [unit: $\rho_{\rm nuc} c^2 / n_{\rm nuc}^\gamma$]
	*/
	double kappa_poly2 ;

	/// Orbital angular velocity [unit: rad/s]
	double omega ;

	/** Distance between the centers (maxiumum density) of the two neutron
	*  stars [unit: km]
	*/
	double dist ;

	/** Distance between the center of masses of two neutron stars
	*  [unit: km]
	*/
	double dist_mass ;

	/** Baryon mass of star 1 (less massive star)
	*  [unit: $M_\odot$]
	*/
	double mass1_b ;

	/** Baryon mass of star 2 (massive star)
	*  [unit: $M_\odot$]
	*/
	double mass2_b ;

	/** ADM mass of the binary system
	*  [unit: $M_\odot$]
	*/
	double mass_adm ;

	/** Total angular momentum of the binary system
	*  [unit: $GM_\odot^2/c$]
	*/
	double angu_mom ;

	/** Coordinate radius of star 1 (less massive star)
	*  parallel to the x axis toward the companion star
	*  [unit: km]
	*/
	double rad1_x_comp ;

	/** Coordinate radius of star 1 (less massive star)
	*  parallel to the y axis [unit: km].
	*/
	double rad1_y ;

	/** Coordinate radius of star 1 (less massive star)
	*  parallel to the z axis [unit: km].
	*/
	double rad1_z ;

	/** Coordinate radius of star 1 (less massive star)
	*  parallel to the x axis opposite to the companion star
	*  [unit: km].
	*/
	double rad1_x_opp ;

	/** Coordinate radius of star 2 (massive star)
	*  parallel to the x axis toward the companion star
	*  [unit: km].
	*/
	double rad2_x_comp ;

	/** Coordinate radius of star 2 (massive star)
	*  parallel to the y axis [unit: km].
	*/
	double rad2_y ;

	/** Coordinate radius of star 2 (massive star)
	*  parallel to the z axis [unit: km].
	*/
	double rad2_z ;

	/** Coordinate radius of star 2 (massive star)
	*  parallel to the x axis opposite to the companion star
	*  [unit: km].
	*/
	double rad2_x_opp ;


	/// Total number of grid points
	int np ;

	/// 1-D array storing the values of coordinate x of the {\tt np} grid points [unit: km]
	double* xx ;

	/// 1-D array storing the values of coordinate y of the {\tt np} grid points [unit: km]
	double* yy ;

	/// 1-D array storing the values of coordinate z of the {\tt np} grid points [unit: km]
	double* zz ;

	/// Lapse function $N$ at the {\tt np} grid points (1-D array)
	double* nnn ;

	/// Component $\beta^x$ of the shift vector of non rotating coordinates [unit: $c$]
	double* beta_x ;

	/// Component $\beta^y$ of the shift vector of non rotating coordinates [unit: $c$]
	double* beta_y ; 

	/// Component $\beta^z$ of the shift vector of non rotating coordinates [unit: $c$]
	double* beta_z ; 

	/// Metric coefficient $\gamma_{xx}$ at the grid points (1-D array)
	double* g_xx ; 

	/// Metric coefficient $\gamma_{xy}$ at the grid points (1-D array)
	double* g_xy ;

	/// Metric coefficient $\gamma_{xz}$ at the grid points (1-D array)
	double* g_xz ; 

	/// Metric coefficient $\gamma_{yy}$ at the grid points (1-D array)
	double* g_yy ; 

	/// Metric coefficient $\gamma_{yz}$ at the grid points (1-D array)
	double* g_yz ; 

	/// Metric coefficient $\gamma_{zz}$ at the grid points (1-D array)
	double* g_zz ; 

	/// Component $K_{xx}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_xx ;

	/// Component $K_{xy}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_xy ;

	/// Component $K_{xz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_xz ;

	/// Component $K_{yy}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_yy ;

	/// Component $K_{yz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_yz ;

	/// Component $K_{zz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
	double* k_zz ;

	// Hydro components
	//------------------
	/** Baryon density in the fluid frame at the {\tt np} grid points (1-D array)
		* [unit: ${\rm kg \, m}^{-3}$]
		*/
	double* nbar ;

	/// Specific internal energy at the  {\tt np} grid points (1-D array) [unit: $c^2$]
	double* ener_spec ;

	/** Component $U^x$ of the fluid 3-velocity with respect to the Eulerian
		* observer, at the {\tt np} grid points (1-D array) [unit: $c$]
		*/
	double* u_euler_x ;

	/** Component $U^y$ of the fluid 3-velocity with respect to the Eulerian
		* observer, at the {\tt np} grid points (1-D array) [unit: $c$]
		*/
	double* u_euler_y ;

	/** Component $U^z$ of the fluid 3-velocity with respect to the Eulerian
		* observer, at the {\tt np} grid points (1-D array) [unit: $c$]
		*/
	double* u_euler_z ;
	
	/*****************************************
	 *  The crucial shared variables
	 *****************************************/
	Lorene::Mg3d *mg1, *mg2;
	Lorene::Map_et *mp1, *mp2;
	
	// These Valeurs are getting new()ed, ie. own storage
	// for subsequent modifications.
	Lorene::Valeur
		*vnn1,
		*vnn2,
		*vacar1,
		*vacar2,
		*vkxx1,
		*vkxx2,
		*vkxy1,
		*vkxy2,
		*vkxz1,
		*vkxz2,
		*vkyy1,
		*vkyy2,
		*vkyz1,
		*vkyz2,
		*vkzz1,
		*vkzz2,
		*vueulerx1,
		*vueulerx2,
		*vueulery1,
		*vueulery2,
		*vueulerz1,
		*vueulerz2;
	
	// These Valeurs are constant references/pointers to
	// storage elsewhere.
	Lorene::Valeur const
		*vshiftx1,
		*vshiftx2,
		*vshifty1,
		*vshifty2,
		*vshiftz1,
		*vshiftz2,
		*vnbar1,
		*vnbar2,
		*vener1,
		*vener2;
	
    /* Just for convenience */
    Lorene::Eos *peos1, *peos2;
	Lorene::Binaire *star;
	FILE* fich;

    // Constructors - Destructor
    // -------------------------
		
	/**
	 * This is the particular feature of this code: There is *no* need to know about
	 * the global positions, interpolation can be done really locally.
	 **/
	BinNS(const char* filename);
	~BinNS();
		
	/**
	 * Interpolate cartesian position x to state vector Q,
	 * using the mappings from variablenames.h
	 **/
	void interpolate(const double* const x, idvars& Q);
};

#endif

#ifndef __LORENE_POINTWISE_READER_MAG_NS__
#define __LORENE_POINTWISE_READER_MAG_NS__

#include <stdio.h> // FILE*

#include "lorene-pointlib/forward-lorene.h"
#include "lorene-pointlib/idvars.h"

namespace LorenePointwise {
    class MagNS;
}

/**
 * Based on magstar-export.cc to patch differentially rotating stars or so. 
 * Magnetized neutron star configuration on a Cartesian grid.
 * Based on MagStar Export code (Export/Mag_NS),
 * 
 * Status of the individual MagNS reader:
 * 
 *      Currently crashing.
 *
 * Feb 2017, Sven Koeppel.
 * Oct 2015, Sven Koeppel <koeppel@fias.uni-frankfurt.de>
 * 
 **/
class LorenePointwise::MagNS  {
    // Data, as in MagNS
    // -----
    public:
	/// Eos name star
	char eos_name[100] ;

	/// Adiabatic index of EOS if it is polytropic (0 otherwise)
	double gamma_poly ;

	/**
	*  Polytropic constant of EOS if it is polytropic (0 otherwise)
	*  [unit: $\rho_{\rm nuc} c^2 / n_{\rm nuc}^\gamma$]
	*/
	double kappa_poly ;

	/// Rotation frequency [unit: rad/s] (only rigid rotation)
	double omega ;

	/// Central density [unit: kg m${}^{-3}$]
	double rho_c ;

	/// Central specific internal energy [unit: c${}^2$]
	double eps_c ;

	/// Baryon mass [unit: solar mass]
	double mass_b ;

	/// Gravitational mass [unit: solar mass]
	double mass_g ;

	/// Coordinate equatorial radius [unit: km]
	double r_eq ;

	/// Coordinate polar radius [unit: km]
	double r_p ;

	/// Angular momentum [unit: $G M_{\textrm{sol}}^2/c$]
	double angu_mom ;

	/// Ratio T/W
	double T_over_W ;

	/// Magnetic momentum [unit: A m${}^2$]
	double magn_mom ;

	/// Magnetic field at the pole [unit: $10^9$ T]
	double b_z_pole ;

	/// Magnetic field at the equator [unit: $10^9$ T]
	double b_z_eq ;


	FILE* fich; ///< input file

	/*** FROM HERE THE REALLY IMPORTANT STUFF SHARED BETWEEN INIT AND INTERPOLATE ***/
	Lorene::Map_et *mapping;
	Lorene::Scalar *sp_lapse, *sp_density, *sp_energy, *sp_jt;
	Lorene::Vector *sp_shift, *sp_u_euler, *sp_current, *sp_Bmag;
	Lorene::Sym_tensor *sp_gamma, *sp_kij;

	/* Just for convenience */
	Lorene::Et_rot_mag *star;
	Lorene::Eos *p_eos;

	MagNS(const char* filename);
	~MagNS();
	
	/**
	 * Interpolate cartesian position x to state vector Q,
	 * using the mappings from variablenames.h
	 **/
	void interpolate(const double* const x, idvars& Q);
	
}; // class

#endif /* __LORENE_POINTWISE_READER_MAG_NS__ */

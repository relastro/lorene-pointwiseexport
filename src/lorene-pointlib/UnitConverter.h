#ifndef LORENE_GEOMETRIC_UNITS_HELPER
#define LORENE_GEOMETRIC_UNITS_HELPER

#include "idvars.h"

/**
 * Define some units, similar to the PizzaNumUtils/unitconv code but
 * slimmer.
 **/
namespace LorenePointwise {
	constexpr double c_SI      = 299792458.0;
	constexpr double G_SI      = 6.673e-11;
	constexpr double M_sun_SI  = 1.98892e30;
	
	// Geometric unit system with the Solar Mass radius [in m] as base
	constexpr double ulength = 1476.7161818921163; // = 1476 m  (not km as base)
	constexpr double utime   = ulength / c_SI;     // = 4.92e-6 s
	constexpr double umass   = ulength * c_SI * c_SI / G_SI; // = 1.989e30 kg

	// derived quantities
	constexpr double udensity = umass / (ulength*ulength*ulength); // = 6.17627e+20 m/kg^3
	
	// and the kilometer in this unit system
	constexpr double km_SI    = 1e3;
	
	// This is just an attemp, maybe overkill.
	template<class Exporter> struct UnitConverter {
		Exporter& e;
		UnitConverter(Exporter& e) : e(e) {}
		void interpolate(const double* const x, idvars& Q) {
			double x_ulorene[3];
			for(int i=0; i<3; i++) x_ulorene[i] = x[i] * ulength/km_SI;
			e.interpolate(x_ulorene, Q);
			Q.nbar /= udensity;
		}
	};
}

#endif /* LORENE_GEOMETRIC_UNITS_HELPER */

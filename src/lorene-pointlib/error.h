#ifndef __LORENE_POINTWISE_ERROR__
#define __LORENE_POINTWISE_ERROR__

#include <stdexcept>
#include <string>

namespace LorenePointwise {
	class error;
}
    
// coming from PizzaBase/src/mesh.h
class LorenePointwise::error: public std::exception {
  const char* msg;
  public:
  error(const char* msg_) : msg(msg_) {}
  virtual const char* what() const throw()
  {
    return msg;
  }
  static void unless(bool c, const std::string& m)
  {
    if (!c) throw error(m.c_str());
  }
  static void incase(bool c, const std::string& m)
  {
    unless(!c,m.c_str());
  }
};

#endif /* __LORENE_POINTWISE_ERROR__ */

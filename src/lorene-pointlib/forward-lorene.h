#ifndef __LORENE_POINTWISE_FORWARD__
#define __LORENE_POINTWISE_FORWARD__

// Forward declaration of LORENE types, so you don't have
// to include LORENE headers when using this.
namespace Lorene {
	class Map_et;
	class Mg3d;
	class Scalar;
	class Vector;
	class Sym_tensor;
	class Etoile_rot;
	class Et_rot_mag;
	class Eos;
	class Valeur;
	class Binaire;
}

#endif /* __LORENE_POINTWISE_FORWARD__ */

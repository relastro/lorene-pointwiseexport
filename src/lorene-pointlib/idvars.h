#ifndef __LORENE_POINTWISE_READER_VECTOR_NAMES__
#define __LORENE_POINTWISE_READER_VECTOR_NAMES__

namespace LorenePointwise {
	struct idvars;
}

#include <string>

/**
 * This structure holds the initial data variables at a single point.
 * Invalid or unset values may be indicated by NaNs, for instance when reading
 * non-magnetized initial data.
 **/
struct LorenePointwise::idvars {
	/// coordinate x,y,z [unit: km]
	double  xx, yy, zz;

	/// Lapse function $N$
	double  nnn;

	/// Components $\beta^i$ of the shift vector of non rotating coordinates [unit: $c$]
	double  beta_x, beta_y, beta_z; 

	/// Metric coefficient $\gamma_{xx}$
	double  g_xx, g_xy, g_xz, g_yy, g_yz, g_zz;

	/// Components of the extrinsic curvature $K_{ij]$ at the grid points [unit: c/km]
	double  k_xx, k_xy, k_xz, k_yy, k_yz, k_zz;

	// Hydro components
	//------------------
	
	/// Baryon density in the fluid frame [unit: kg m^{-3}]
	double  nbar;

	/// Specific internal energy [unit: $c^2$]
	double  ener_spec;

	/**
	 * Component $U^x, U^y, U^z$ of the fluid 3-velocity with respect to the Eulerian
	 * observer [unit: $c$]
	 **/
	double  u_euler_x, u_euler_y, u_euler_z;

	/// Component $B^x, B^y, B^z$ of the magnetic field [unit: 10^9 T]
	double  bb_x, bb_y, bb_z;

	/// Component $j^t$ of the 4-current[unit: A m^-2]
	double  jj_t;

	/// Component $j^x, j^y, j^z$ of the 4-current [unit: A m^-2]
	double  jj_x, jj_y, jj_z;

	/// Give a human-readable string representation of all members
	std::string toString();
}; // idvars

#endif /* __LORENE_POINTWISE_READER_VECTOR_NAMES__ */
